```python
def fonction(x):
    return x**2
```

$T =$ variable aléatoire donnant la durée de vie d'un atome.

# Point de vue probabiliste

**Modèle de désintégration = durée de vie sans vieillissement** :

*La probabilité qu'un atome soit « vivant » à l'intant $t + h$ sachant qu'il 
est « vivant » à l'instant $t$ ne dépend pas de $t$ (quel que soit $h$) :*




$$ P_{(T > t)} (T > t + h) = P (T > t) $$

On en tire que1em: $P (T > t + h) = P (T > t) \times P (T > h)$ 
pour tout $t, h$

La fonction $G (x) = P (T > x)$ vérifie donc la propriété : $G (x + y) = G 
(x) \times G (y)$ pour $x, y \geqslant 0$

Alors on peut alors démontrer qu'une telle fonction est une exponentielle. 
Plus précisément, il existe un réel $\lambda > 0$ ($> 0$ car une 
probabilité $\in [0 ; 1]$) tel que $G (x) = e^{\lambda x}$.

On a donc $P (T > t) = e^{-\lambda t} \Longleftrightarrow P (T \leqslant t) = 1 - e^{- \lambda t}$.

Ce sont les formules de calcul des probabilités d'une loi exponentielle à 
densité $f (t) = \lambda e^{-\lambda t}$.

La valeur moyenne dans une loi exponentielle est $\dfrac{1}{\lambda}$. C'est donc la durée moyenne de vie d'**un** **atome** avec ce modèle.



# Point de vue statistique
![un histogramme](images/screen-2023-06-28-16-37-45.png)

[voir une explication](https://www.cea.fr/comprendre/Pages/radioactivite/radioactivite.aspx?Type=Chapitre&numero=1)


En statistique, on réfléchit sur un échantillon.

Si on appelle $N (t)$ le nombre d'atomes vivants à l'instant $t$, on doit 
plutôt considérer ce nombre comme une moyenne, la moyenne de $N_{0}$ 
variables aléatoires dont chacune vaut 1 si l'atome est vivant et 0 sinon (=la 
valeur moyenne d'une variable suivant une loi binômiale).

Il faut d'abord préciser le modèle précédent  :

* chaque atome se désintègre sans vieillissement
* les atomes ne s'influencent pas et au départ, on en a $N_{0}$.

A l'instant $t$, chaque atome a une probabilité $P (T > t) = e^{-\lambda 
t}$ d'être vivant. Comme on en a $N_{0}$, le nombre de survivants sera 
en moyenne $N_{0} e^{-\lambda t}$. (loi binomiale : nombre répétitions 
× proba succès))

En gros, on peut considérer $\dfrac{N (t)}{N_{0}}$ comme la proportion de 
survivants à l'instant $t$ et elle vaut  $P(T \geq t) = e^{-\lambda t}$.


$$\sqrt{x^2+1} \neq x+1$$

Et une insertion en ligne donnera : $\sqrt{x^2+1}\neq x+1$


On mesure les durées de vie d'atomes dans un échantillon et on place les 
résultats dans un histogramme : (en ordonnées: %tage de survivants à l'instant 
$t$)





Sur un échantillon, on voit que les sommets des barres semblent suivre une 
fonction cste × exponentielle (en fait la densité). En effet, $\hat{N}(t) \approx N (t) = N_{0} e^{-\lambda t}$


|  1 atome |  2 atome  | 3 atomes  | 4 atomes | etc ..  |
|--:|---|---|---|---|
|  2 | 4  | 8  | 9  |1   |
| $f$  |   |   |   |   |
|  $f$ |   |   |   |   |


# Point de vue différentiel

La vitesse instantannée de désintégration est :



où $N(t)$ est la variable aléatoire donnant le nombre moyen d'atomes vivants 
à l'instant $t$ .


Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.







